<?php

namespace Drupal\csv_importer_commerce\Plugin\Importer;

use Drupal\csv_importer_commerce\Plugin\ImporterBase;

/**
 * Class CommerceProductVariationImporter.
 *
 * @Importer(
 *   id = "commerce_product_variation_importer",
 *   entity_type = "commerce_product_variation",
 *   label = @Translation("Commerce Product Variation importer")
 * )
 */
class CommerceProductVariationImporter extends ImporterBase {}
