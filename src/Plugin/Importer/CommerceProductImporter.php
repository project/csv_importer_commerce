<?php

namespace Drupal\csv_importer_commerce\Plugin\Importer;

use Drupal\csv_importer_commerce\Plugin\ImporterBase;

/**
 * Class CommerceProductImporter.
 *
 * @Importer(
 *   id = "commerce_product_importer",
 *   entity_type = "commerce_product",
 *   label = @Translation("Commerce Product importer")
 * )
 */
class CommerceProductImporter extends ImporterBase {}
